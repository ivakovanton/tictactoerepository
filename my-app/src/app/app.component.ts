import { Component, Input } from '@angular/core';
import { GameService } from './service/game.service';
import { StorageService } from './service/storage.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GameService, StorageService]
})
export class AppComponent {

}