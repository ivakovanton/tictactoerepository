import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UserComponent } from './gameItem/gameItem.component';
import { GameComponent } from './game/game.component';
import { LobbyComponent } from './lobby/lobby.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes =[
    { path: '', component: LobbyComponent},
    { path: 'game/:token', component: GameComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    GameComponent,
    LobbyComponent
  ],
  imports: [
    FormsModule, BrowserModule, RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }