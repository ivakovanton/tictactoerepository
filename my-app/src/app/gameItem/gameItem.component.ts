import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { LobbyComponent } from '../lobby/lobby.component';
import { GameService } from '../service/game.service';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-gameItem',
  templateUrl: './gameItem.component.html',
  styleUrls: ['./gameItem.component.css']
})


export class UserComponent implements OnInit {
    @Input() game;

    statePlaying = false;
    stateDone    = false;
    stateReady   = true;

    ownerWin  = false;
    ownerLoss = false;

    opponentWin  = false;
    opponentLoss = false;

    invisibleOwner    = true;
    invisibleOpponent = true;
    
    constructor(
        private router: Router,
        private lobbyComponent: LobbyComponent,
        private gameService: GameService,
        private storageService: StorageService
    ) {}
    
    changeClassOfGames() {
        let state = this.storageService.get(this.game.token).state;
        if (state == this.gameService.GAME_STATE_PLAYING){
            this.statePlaying = true;
            this.stateDone    = false;
            this.stateReady   = false;
        };        
        if (state == this.gameService.GAME_STATE_DONE){
            this.statePlaying = false;
            this.stateDone = true;
            this.stateReady = false;
        };        
    }
    
    changeClassOfStatusOfPlayers() {
        let result = this.storageService.get(this.game.token).result;
        if (result == this.gameService.RESULT_OWNER_WIN){
            this.ownerWin = true;
            this.opponentLoss = true;
            this.invisibleOwner = false;
        };
        if (result == this.gameService.RESULT_OPPONENT_WIN){
            this.ownerLoss = true;
            this.opponentWin = true;
            this.invisibleOpponent = false;
        };
        if (result == this.gameService.RESULT_DRAW){
            this.ownerWin = true;
            this.opponentWin = true;
            this.invisibleOwner = true;
            this.invisibleOpponent = true;
        }; 
    }
    
    isOpponentComeInTheGame(token) {
        return this.storageService.get(token).opponent == undefined && 
        this.storageService.get(token).owner != undefined        
    }
    
    isComeInViewMode(token) {
        return this.storageService.get(token).owner != undefined &&
        this.storageService.get(token).opponent != undefined       
    }
    
    secondPlayerComeInTheGame(event) {
        let token = event.target.id || event.target.closest(".game-element").id;
        let owner = this.storageService.get(token).owner;
        if (this.isOpponentComeInTheGame(token)){
            this.router.navigate([this.gameService.ROUTE_GAME_TIC_TAC_TOE, token], {
                queryParams: {
                    userName: this.lobbyComponent.userName
                }
            });
        };
        if (this.isComeInViewMode(token)) {
            this.router.navigate([this.gameService.ROUTE_GAME_TIC_TAC_TOE, token]);
        };
    }
    
    ngOnInit() {
        this.changeClassOfGames();
        this.changeClassOfStatusOfPlayers();
    }
}
