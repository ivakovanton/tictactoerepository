import { Injectable } from '@angular/core';
import { Input, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GameService } from '../service/game.service';
import { StorageService } from '../service/storage.service';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.css']
})
@Injectable()

export class GameComponent implements OnInit {
    token: any;
    userName: any;    
    gameOwner: any;
    gameOpponent: any;
    
    moveOwner = true;
    moveOpponent = false;
    isViewMode = false;

    gameObject;
    graphicContext;
    refreshGameStateTimer;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private gameService: GameService,
        private storageService: StorageService
    ) { 
        activatedRoute.params.subscribe((params : any)      => this.token    = params.token);
        activatedRoute.queryParams.subscribe((params : any) => this.userName = params.userName);
    }
    
    makeMove(event) {
        this.gameService.makeMove(event, this.token, this.graphicContext, this.userName);
    }
    
    surrender() {
        let objectGame = this.storageService.get(this.token);

        if (this.userName == this.storageService.get(this.token).owner) {
            objectGame.result = this.gameService.RESULT_OPPONENT_WIN;
            objectGame.state = this.gameService.GAME_STATE_DONE;
            this.storageService.setItem(this.token, objectGame);
            this.gameService.exitFromTheFinishedGame(this.token, this.refreshGameStateTimer, this.router);
        };
        if (this.userName == this.storageService.get(this.token).opponent) {
            objectGame.result = this.gameService.RESULT_OWNER_WIN;
            objectGame.state = this.gameService.GAME_STATE_DONE;
            this.storageService.setItem(this.token, objectGame);
            this.gameService.exitFromTheFinishedGame(this.token, this.refreshGameStateTimer, this.router);
        }; 
        if (this.gameService.isViewMode(this.userName, this.token)) {
            clearInterval(this.refreshGameStateTimer);
            this.router.navigate([this.gameService.ROUTE_GAME_LOBBY]);
        }
    }
    
    getRefreshGameStateTimer(){
        let self = this;
        let objectGame = self.storageService.get(self.token);
        if (self.ownerOrOpponentSaveInTheGame()) {
            self.gameOwner = self.storageService.get(self.token).owner;
            objectGame.turn = self.storageService.get(self.token).turn;
            self.gameService.fillGameField(self.token, self.graphicContext, self.userName);
            if (self.opponentSaveInTheGame()){
                self.gameOpponent = self.storageService.get(self.token).opponent;
            };
            if (objectGame.turn%2==0) {
                self.moveOwner = true;
                self.moveOpponent = false;
            }
            else  {
                self.moveOpponent = true;
                self.moveOwner = false;
            };
        };   
        objectGame.time++;
        if (self.isPageOfOwner()) {   
            self.storageService.setItem(self.token, objectGame);
        };
        objectGame.time = self.storageService.get(self.token).time;
        self.gameService.setTimer(objectGame);
        self.gameService.checkingActivityInTheGame(objectGame, self.refreshGameStateTimer, self.router);
        self.gameService.exitFromTheFinishedGame(self.token, self.refreshGameStateTimer, self.router);
    }
    
    newPlayerNotSaveInTheGame() {
        return this.storageService.get(this.token) == null || this.storageService.get(this.token).opponent == null
    }
    
    ownerOrOpponentSaveInTheGame() {
        return this.storageService.get(this.token) != null
    } 
    
    opponentSaveInTheGame() {
        return this.storageService.get(this.token).opponent != null
    } 
    
    isPageOfOwner() {
        return this.userName == this.storageService.get(this.token).owner
    }
    
    ngOnInit() {
        let self = this;
        let canvas = <HTMLCanvasElement> document.getElementById('canvas');
        this.graphicContext = canvas.getContext('2d');
        this.gameObject = this.gameService.getDataForGameObject(this.token, this.userName);
        
        this.gameService.drawGameField(this.graphicContext, this.userName, this.token);
        
        if (this.newPlayerNotSaveInTheGame()) {
            this.storageService.setItem(this.token, this.gameObject)            
        };
        if (this.gameService.isViewMode(this.userName, this.token)) {
            this.isViewMode = true;
            document.getElementById("SURRENDER").setAttribute('value', "BACK");
        };
        this.refreshGameStateTimer = setInterval(function () {
            self.getRefreshGameStateTimer();
        }, self.gameService.UPDATE_TIME); 
    }
}
