import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../service/game.service';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})

export class LobbyComponent implements OnInit {
    userName: string;
    games = [];
    gameListRefreshTimer;
    
    constructor(
        private router: Router,
        private gameService: GameService,
        private storageService: StorageService
    ) {}
    
    ngOnInit() {
        let self = this;
        this.gameListRefreshTimer = setInterval(function () {
            self.games = self.storageService.getAllGames();
        }, self.gameService.UPDATE_TIME);
    }
    
    onSubmit() {
        clearInterval(this.gameListRefreshTimer);
        let token = Math.random().toString(36);
        this.router.navigate([this.gameService.ROUTE_GAME_TIC_TAC_TOE, token], {
            queryParams: {
                userName: this.userName
            }
        });
    }
}
