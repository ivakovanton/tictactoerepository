import {Injectable} from '@angular/core';

@Injectable()

export class StorageService {
    
    constructor(
    ) {}
    
    getAllGames() {
        let key = [];
        let dataGame = [];
        for (let i = 0; i < localStorage.length; i++) { 
            key[i] = localStorage.key(i);
            dataGame[i] = this.get(key[i]);
        }; 
        return dataGame;
    }        
    
    get(token) {
        return JSON.parse(localStorage.getItem(token));
    }

    setItem(token, objectGame) {
        return localStorage.setItem(token, JSON.stringify(objectGame));
    }

    removeItem(token) {
        return localStorage.removeItem(token);
    } 
}