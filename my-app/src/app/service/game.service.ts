import {Injectable} from '@angular/core';
import { StorageService } from './storage.service';

@Injectable()

export class GameService {
    GAME_STATE_READY   = 'ready';
    GAME_STATE_PLAYING = 'playing';
    GAME_STATE_DONE    = "done";

    RESULT_DRAW = 'draw';
    RESULT_OWNER_WIN = 'owner';
    RESULT_OPPONENT_WIN = 'opponent';
    
    CROSS_SYMBOL = 'X';
    ZERO_SYMBOL = '0';
    CROSS_WIN = 'Победили крестики';
    ZERO_WIN = 'Победили нолики';
    DRAW = 'Ничья';
    LACK_OF_ACTIVITY = 'Нет активности в игре';
    TURN_SUM = '9';
    EMPTY_CELL = '_';    

    MAXIMUM_INACTIVITY_SECONDS = 300;
    CELL_SIZE = 100;
    SIZE_OF_HALF_THE_CELL = 50;
    LINE_WIDTH = 4;
    RADIUS_SYMBOL = 30;
    GAME_FIELD_SIZE = 300;
    GAME_FIELD_ZERO = 0;
    THIRD_OF_GAME_FIELD_SIZE = 100;
    TWO_THIRD_OF_GAME_FIELD_SIZE = 200;
    UPDATE_TIME = 1000;
    NUMBER_OF_SIGNIFICANT_SYMBOL = 3;
    FIRST_CELL_NUMBER = 0;
    WITHOUT_OFFSET = 0;

    ZERO_COLOR = "#5b9ade";
    CROSS_COLOR = "#777";
    GAME_FIELD_COLOR = "#f0ad4e";
    GAME_FIELD_COLOR_IN_VIEW_MODE = "#ddd";
    ZERO_COLOR_IN_VIEW_MODE = "#777";
    CROSS_COLOR_IN_VIEW_MODE = "#777";
    
    ROUTE_GAME_LOBBY = '';
    ROUTE_GAME_TIC_TAC_TOE = "/game";

    constructor(
        private storageService: StorageService
    ) {}
    
    getDataForGameObject(token, userName) {
        let gameObject:any = {};
        gameObject.token = token;
        gameObject.result = 0;
        gameObject.turn = 0;
        gameObject.time = '';
        gameObject.hour = '';
        gameObject.minute = '';
        gameObject.second = '';
        gameObject.state = this.GAME_STATE_READY;
        gameObject.array = this.createGameField();
        
        if (this.ownerInTheGameOnHisPage(token, userName)) {
            gameObject.owner = userName;
        }
        else {
            gameObject.owner = this.storageService.get(token).owner;
            gameObject.opponent = userName;
            gameObject.array = this.storageService.get(token).array;
            gameObject.turn = this.storageService.get(token).turn;
        };
        return gameObject;
    }
    
    createGameField() {
        let gameArray=[];
        for (let i = 0; i < 3; i++){
            gameArray[i] = [];
            for (let j = 0; j < 3; j++){
                gameArray[i][j] = this.EMPTY_CELL;
            };
        };
        return gameArray;
    }
    
    drawGameField(graphicContext, userName, token) {
        graphicContext.beginPath();
        graphicContext.lineWidth = this.LINE_WIDTH;
        if (this.isViewMode(userName, token)) {
            graphicContext.strokeStyle = this.GAME_FIELD_COLOR_IN_VIEW_MODE;
        }
        else {
            graphicContext.strokeStyle = this.GAME_FIELD_COLOR;
        };
        graphicContext.moveTo(this.GAME_FIELD_ZERO, this.THIRD_OF_GAME_FIELD_SIZE);
        graphicContext.lineTo(this.GAME_FIELD_SIZE, this.THIRD_OF_GAME_FIELD_SIZE);
        
        graphicContext.moveTo(this.GAME_FIELD_ZERO, this.TWO_THIRD_OF_GAME_FIELD_SIZE);
        graphicContext.lineTo(this.GAME_FIELD_SIZE, this.TWO_THIRD_OF_GAME_FIELD_SIZE);
        
        graphicContext.moveTo(this.THIRD_OF_GAME_FIELD_SIZE, this.GAME_FIELD_ZERO);
        graphicContext.lineTo(this.THIRD_OF_GAME_FIELD_SIZE, this.GAME_FIELD_SIZE);
        graphicContext.moveTo(this.TWO_THIRD_OF_GAME_FIELD_SIZE, this.GAME_FIELD_ZERO);
        graphicContext.lineTo(this.TWO_THIRD_OF_GAME_FIELD_SIZE, this.GAME_FIELD_SIZE);
        graphicContext.stroke(); 
    }
    
    drawZero(graphicContext, userName, token, offsetX, offsetY){
        graphicContext.beginPath();
        if (this.isViewMode(userName, token)) {
            graphicContext.strokeStyle = this.ZERO_COLOR_IN_VIEW_MODE;
        }
        else {
            graphicContext.strokeStyle = this.ZERO_COLOR;
        };
		let centerX = this.CELL_SIZE * offsetX + this.SIZE_OF_HALF_THE_CELL;
		let centerY = this.CELL_SIZE * offsetY + this.SIZE_OF_HALF_THE_CELL;
		let startingAngle = 0;
		let endingAngle = Math.PI * 2;
		graphicContext.moveTo(centerX + this.RADIUS_SYMBOL,centerY);
		graphicContext.arc(centerX, centerY, this.RADIUS_SYMBOL, startingAngle, endingAngle);
		graphicContext.stroke();
    }
    
    drawCross(graphicContext, userName, token, offsetX, offsetY){
        graphicContext.beginPath();
        if (this.isViewMode(userName, token)) {
            graphicContext.strokeStyle = this.CROSS_COLOR_IN_VIEW_MODE;
        }
        else {
            graphicContext.strokeStyle = this.CROSS_COLOR;
        };
		let centerX = this.CELL_SIZE * offsetX + this.SIZE_OF_HALF_THE_CELL;
		let centerY = this.CELL_SIZE * offsetY + this.SIZE_OF_HALF_THE_CELL; 
        graphicContext.moveTo(centerX - this.RADIUS_SYMBOL, centerY - this.RADIUS_SYMBOL); 
        graphicContext.lineTo(centerX + this.RADIUS_SYMBOL, centerY + this.RADIUS_SYMBOL);
        graphicContext.moveTo(centerX - this.RADIUS_SYMBOL, centerY + this.RADIUS_SYMBOL); 
        graphicContext.lineTo(centerX + this.RADIUS_SYMBOL, centerY - this.RADIUS_SYMBOL);
        graphicContext.stroke();
    } 
    
    fillGameField(token, graphicContext, userName){
        let gameArray = this.storageService.get(token).array;
        if (gameArray == []){
            return false;
            }
        else {
            for (let j = 0; j < 3; j++){
                for (let i = 0; i < 3; i++){
                    if (gameArray[j][i] == this.CROSS_SYMBOL){
                        this.drawCross(graphicContext, userName, token, i, j)
                    }
                    if (gameArray[j][i] == this.ZERO_SYMBOL){ 
                        this.drawZero(graphicContext, userName, token, i, j)
                    }
                }                
            }
        }           
    }
    
    getCellOffsetX(event) {
        let offsetX = event.offsetX;
        return offsetX = Math.floor(offsetX/100);        
    }
    
    getCellOffsetY(event) {
        let offsetY = event.offsetY;
        return offsetY = Math.floor(offsetY/100);        
    }
    
    makeMove(event, token, graphicContext, userName) {
        let gameObject = this.storageService.get(token);
        gameObject.state = this.GAME_STATE_PLAYING;
        let offsetX = this.getCellOffsetX(event);
        let offsetY = this.getCellOffsetY(event);
            
        if (gameObject.turn%2 == 0) {
            if (this.isEventOfOwner(gameObject, userName, offsetX, offsetY)) {
                gameObject.array[offsetY][offsetX] = this.CROSS_SYMBOL;
                this.drawCross(graphicContext, userName, token, offsetX, offsetY);
            }
            else {
                gameObject.turn = gameObject.turn-1;
                this.storageService.setItem(token, gameObject);
            }
        }
        else  {
            if (this.isEventOfOpponent(gameObject, userName, offsetX, offsetY)) {
                gameObject.array[offsetY][offsetX] = this.ZERO_SYMBOL;
                this.drawZero(graphicContext, userName, token, offsetX, offsetY);
            }
            else {
                gameObject.turn = gameObject.turn-1;
                this.storageService.setItem(token, gameObject);
            }
        };
        gameObject.turn++;
        this.storageService.setItem(gameObject.token, gameObject);
        this.checkWin(gameObject, offsetX, offsetY);
    }
    
    winSymbol(gameObject, offsetX, offsetY) {
        if (gameObject.array[offsetY][offsetX] == this.CROSS_SYMBOL) {
            alert(this.CROSS_WIN);
            gameObject.result = this.RESULT_OWNER_WIN;
            gameObject.state = this.GAME_STATE_DONE;
            this.storageService.setItem(gameObject.token, gameObject);
        }
        if (gameObject.array[offsetY][offsetX] == this.ZERO_SYMBOL){
            alert(this.ZERO_WIN);
            gameObject.result = this.RESULT_OPPONENT_WIN;
            gameObject.state = this.GAME_STATE_DONE;
            this.storageService.setItem(gameObject.token, gameObject);            
        }
    }
    
    draw(gameObject) {
        alert(this.DRAW);
        gameObject.result = this.RESULT_DRAW;
        gameObject.state = this.GAME_STATE_DONE;
        this.storageService.setItem(gameObject.token, gameObject);        
    }
    
    checkResult(sumSymbol, gameObject, offsetX, offsetY) {
        if (sumSymbol == this.NUMBER_OF_SIGNIFICANT_SYMBOL) {
            this.winSymbol(gameObject, offsetX, offsetY)
        };        
    }

    checkAxisY(gameObject, offsetX, offsetY) {
        let sumSymbol = 1;
        let i = 1;
        while (this.isNotStartFieldBorder(offsetY - i) && this.areSymbolsEqual(gameObject, offsetX, offsetY, this.WITHOUT_OFFSET, -i)) {
            sumSymbol++;
            i++;
        };
        i = 1;
        while (this.isNotEndFieldBorder(offsetY + i, gameObject) && this.areSymbolsEqual(gameObject, offsetX, offsetY, this.WITHOUT_OFFSET, i)) {
            sumSymbol++;
            i++;
        };

        this.checkResult(sumSymbol, gameObject, offsetX, offsetY);      
    }
    
    checkAxisX(gameObject, offsetX, offsetY) {
        let sumSymbol = 1;
        let i = 1;
        while (this.isNotStartFieldBorder(offsetX - i) && this.areSymbolsEqual(gameObject, offsetX, offsetY, -i, this.WITHOUT_OFFSET)) {
            sumSymbol++;
            i++;
        };
        i = 1;
        while (this.isNotEndFieldBorder(offsetX + i, gameObject) && this.areSymbolsEqual(gameObject, offsetX, offsetY, i, this.WITHOUT_OFFSET)) {        
            sumSymbol++;
            i++;
        }; 
        
        this.checkResult(sumSymbol, gameObject, offsetX, offsetY);     
    } 
    
    checkFirstDiagonal(gameObject, offsetX, offsetY) {
        let sumSymbol = 1;
        let i = 1;
        while (this.isNotStartFieldBorder(offsetX - i) && this.isNotStartFieldBorder(offsetY - i) && this.areSymbolsEqual(gameObject, offsetX, offsetY, -i, -i)) {
            sumSymbol++;
            i++;
        };
        i = 1;
        while (this.isNotEndFieldBorder(offsetX + i, gameObject) && this.isNotEndFieldBorder(offsetY + i, gameObject) && this.areSymbolsEqual(gameObject, offsetX, offsetY, i, i)) {
            sumSymbol++;
            i++;
        }; 
        
        this.checkResult(sumSymbol, gameObject, offsetX, offsetY);         
    }
    
    checkSecondDiagonal(gameObject, offsetX, offsetY) {
        let sumSymbol = 1;
        let i = 1;
        while (this.isNotEndFieldBorder(offsetX + i, gameObject) && this.isNotStartFieldBorder(offsetY - i) && this.areSymbolsEqual(gameObject, offsetX, offsetY, i, -i)) {
            sumSymbol++;
            i++;
        };
        i = 1;
        while (this.isNotStartFieldBorder(offsetX - i) && this.isNotEndFieldBorder(offsetY + i, gameObject) && this.areSymbolsEqual(gameObject, offsetX, offsetY, -i, i)) {
            sumSymbol++;
            i++;
        }; 
        
        this.checkResult(sumSymbol, gameObject, offsetX, offsetY);         
    }    

    checkWin(gameObject, offsetX, offsetY) {
        this.checkAxisY(gameObject, offsetX, offsetY);
        this.checkAxisX(gameObject, offsetX, offsetY);
        this.checkFirstDiagonal(gameObject, offsetX, offsetY);
        this.checkSecondDiagonal(gameObject, offsetX, offsetY);
        if (gameObject.state == this.GAME_STATE_PLAYING && gameObject.turn == this.TURN_SUM) {
            this.draw(gameObject);
        };
    }
    
    getHours(time) {
        let hour = Math.floor(parseInt(time, 10) / 3600);
        if (hour < 1) {return hour = 0}
        else {return hour}       
    }
    
    getMinutes(time, hour) {
        let minute = Math.floor((parseInt(time, 10) - hour * 3600) / 60);
        if (minute < 1 ) {return minute = 0}
        else {return minute}        
    }
    
    getSeconds(time, hour, minute) {
        let second = (parseInt(time, 10) - minute * 60 - hour * 3600);
        return second;
    }
    
    getTimerHour(hour) {
        if (hour < 10) {return hour = '0'+hour.toString()}
        else {return hour};
    }
    
    getTimerMinute(minute) {
        if (minute < 10) {return minute = '0'+minute.toString()}
        else {return minute};   
    }
    
    getTimerSecond(second) {
        if (second < 10 ) {return second = '0'+second.toString()}
        else {return second};        
    } 
    
    setTimer(objectGame) {
        let time = this.storageService.get(objectGame.token).time;
        let hour:any = this.getHours(time);
        let minute:any = this.getMinutes(time, hour);
        let second:any = this.getSeconds(time, hour, minute);
        hour = this.getTimerHour(hour);
        minute = this.getTimerMinute(minute);
        second = this.getTimerSecond(second);
        objectGame.hour = hour;
        objectGame.minute = minute;
        objectGame.second = second; 
        this.storageService.setItem(objectGame.token, objectGame);
        let getElement = document.getElementById('timer');
        if (getElement) {
            getElement.innerHTML = '<span class="timer">'+hour+':'+minute+':'+second+'</span>';
        }
    }
    
    checkingActivityInTheGame(objectGame, refreshGameStateTimer, router){
        if (this.isNotActivityInTheGame(objectGame)) {
            objectGame.state = this.GAME_STATE_DONE;
            this.storageService.setItem(objectGame.token, objectGame);
            clearInterval(refreshGameStateTimer);
            alert(this.LACK_OF_ACTIVITY);
            router.navigate([this.ROUTE_GAME_LOBBY]);
            this.storageService.removeItem(objectGame.token);
        }; 
    }
    
    getOutFromTheGame(refreshGameStateTimer, router) {
        clearInterval(refreshGameStateTimer);
        router.navigate([this.ROUTE_GAME_LOBBY]);        
    }
    
    exitFromTheFinishedGame(token, refreshGameStateTimer, router) {
        if (this.isEndOfGame(token)){
            this.getOutFromTheGame(refreshGameStateTimer, router)
        }; 
    }
    
    isNotStartFieldBorder(offset) {
        return offset >= this.FIRST_CELL_NUMBER;
    }

    isNotEndFieldBorder(offset, gameObject) {
        return offset <= gameObject.array.length-1;
    }
    
    areSymbolsEqual(gameObject, cellX, cellY, offsetX, offsetY) {
        return gameObject.array[cellY + offsetY][cellX + offsetX] == gameObject.array[cellY][cellX];
    }    

    ownerInTheGameOnHisPage(token, userName) {
        return this.storageService.get(token) == null || userName == this.storageService.get(token).owner
    }
    
    isNotActivityInTheGame(objectGame) {
        return objectGame.time == this.MAXIMUM_INACTIVITY_SECONDS && 
        this.storageService.get(objectGame.token).state == this.GAME_STATE_READY
    }
    
    isEndOfGame(token) {
        return this.storageService.get(token) != null && this.storageService.get(token).state == this.GAME_STATE_DONE
    }
    
    isEventOfOwner(gameObject, userName, i, j) {
        return gameObject.array[j][i] == this.EMPTY_CELL && userName == this.storageService.get(gameObject.token).owner
    }
    
    isEventOfOpponent(gameObject, userName, i, j) {
        return gameObject.array[j][i] == this.EMPTY_CELL && userName == this.storageService.get(gameObject.token).opponent
    }
    
    isViewMode(userName, token) {
        return userName == undefined && this.storageService.get(token).opponent != undefined;
    }    
}    